/********************************************************************************
** Form generated from reading UI file 'dialog_message_popup_window.ui'
**
** Created: Mon Feb 18 13:31:25 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_MESSAGE_POPUP_WINDOW_H
#define UI_DIALOG_MESSAGE_POPUP_WINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_dialog_message_popup_window
{
public:
    QPushButton *pushButton;
    QLabel *label;

    void setupUi(QDialog *dialog_message_popup_window)
    {
        if (dialog_message_popup_window->objectName().isEmpty())
            dialog_message_popup_window->setObjectName(QString::fromUtf8("dialog_message_popup_window"));
        dialog_message_popup_window->resize(168, 139);
        pushButton = new QPushButton(dialog_message_popup_window);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(30, 100, 101, 31));
        label = new QLabel(dialog_message_popup_window);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 30, 241, 71));

        retranslateUi(dialog_message_popup_window);

        QMetaObject::connectSlotsByName(dialog_message_popup_window);
    } // setupUi

    void retranslateUi(QDialog *dialog_message_popup_window)
    {
        dialog_message_popup_window->setWindowTitle(QApplication::translate("dialog_message_popup_window", "Dialog", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("dialog_message_popup_window", "Okay", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("dialog_message_popup_window", "TextLabel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class dialog_message_popup_window: public Ui_dialog_message_popup_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_MESSAGE_POPUP_WINDOW_H
