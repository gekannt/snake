#ifndef MACRO_H
#define MACRO_H

#include <errno.h>

#define NDEBUG 2
#undef NDEBUG

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ... ) fprintf (stderr,"DEBUG %s:%d "M "\n",__FILE__,__LINE__,##__VA_ARGS__)
#endif


// FORMAT OF concatenation of a few strings
// log_warn("something %s", "go  on");



// reports about usual operations will be directed to stdout
#define log_info(M, ...)  fprintf(stdout,"[INFO] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)


// information about something crucial that went wrong will go to stderr

#define log_err(M, ...)  fprintf(stderr,"[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define log_warn(M, ...)  fprintf(stderr,"[WARN] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)

#define check(A,M, ...) if (!(A)) { log_err(M, ##__VA_ARGS__); errno=0; goto error; }
#define clean_errno() (errno==0 ? "None":strerror(errno))


#define check_mem(A)  check ( (A),"Out of memory");

#define check_debug(A,M,...) if ( !(A)) { debug(M, ##__VA_ARGS) }

#define sentinel(M, ...) { log_err(M, ##__VA_ARGS__); errno=0; goto error; }



#define ERROR_MESSAGES_FILE "errlog.txt"
#define OUTPUT_MESSAGES_FILE "outlog.txt"

#define REDIRECT_TO_FILES()\
{ \
    if ( freopen(OUTPUT_MESSAGES_FILE,"w+",stdout) == NULL) \
    {  printf("logout can't be opened\n"); \
        return 0; \
    } \
    if(  freopen(ERROR_MESSAGES_FILE,"w+",stderr) == NULL)\
    { printf("logerr can't be opened\n");\
      return 0;\
    }\
}

//yes, I was just a kid with my macros
#define GENERATE_MESSAGE(a) \
{\
  cerr<<a<<"\n"<<__FILE__<<"  "<<__LINE__<<"\n"; \
}

#define GENERATE_ERROR() \
{ cerr<<"error in"<<__FILE__<<"  "<<__LINE__<<"\n"; \
  exit(0);  \
}

#define GENERATE_ERROR_VERBOSE(a) \
{\
  cerr<<a<<"\n"<<__FILE__<<"  "<<__LINE__<<"\n"; \
}





#endif // MACRO_H
