#include "snake_class.h"

const int step =15;

Updating::Updating()
{
   this->setPen(QPen(QColor("black")));
   mmessage="game is over";
    mx=my=0;
    last_action_y=-15;
    last_action_x=0;
    msizex_ellipse=msizey_ellipse=10;
  mcollapsed=false;
  this->advance(1);
}



int Updating::should_be_cleared()
{
    return mcollapsed;
}


void Updating::collapse()
{
  mcollapsed=true;
}


void Updating::advance(int phase)
 {
   this->setPen(QPen(QColor("black")));
   this->setBrush(QBrush (QColor("black")));

   mpainter_path.addEllipse(mx,my,msizex_ellipse,msizey_ellipse);

   qDebug()<<mx<<" "<<my;

   for( iter=mgone_path.begin(); iter!=mgone_path.end(); iter++)
   {
       if ( mx==iter->x() && my==iter->y() )
       {  collapse();
          dialog_message_popup_window message;
          message.mshow(mmessage.c_str());
       }

   }
   mgone_path.push_back(QPoint(mx,my));


   setPath(mpainter_path);
 }


void Updating::move_up()
{
  this->setPen(QPen(QColor(qrand()%255,qrand()%255,qrand()%255)));

if (last_action_y!=step)
    {  my-=step;
       last_action_y=-step;
       last_action_x=0;
       this->advance(1);
   }

}


void Updating::move()
{


  my+=last_action_y;
  mx+=last_action_x;

  qDebug()<<mx<<" "<<my;


  for( iter=mgone_path.begin(); iter!=mgone_path.end(); iter++)
  {
      if ( mx==iter->x() && my==iter->y() )
      {
          collapse();
          dialog_message_popup_window message;
          message.mshow(mmessage.c_str());



      }

  }

 mgone_path.push_back(QPoint(mx,my));
 mpainter_path.addEllipse(mx,my,msizex_ellipse,msizey_ellipse);



 setPath(mpainter_path);
}

void Updating::move_down()
{

   this->setPen(QPen(QColor(qrand()%255,qrand()%255,qrand()%255)));

 if(last_action_y!=-step)
    {   my+=15;
        last_action_y=step;
        last_action_x=0;
        this->advance(1);
    }
 }


void Updating::move_right()
{
    this->setPen(QPen(QColor(qrand()%255,qrand()%255,qrand()%255)));

    if(last_action_x!=step)
    {
       mx-=step;
      last_action_x=-step;
      last_action_y=0;

      this->advance(1);
    }
}


void Updating::move_left()
{
    this->setPen(QPen(QColor(qrand()%255,qrand()%255,qrand()%255)));
    if(last_action_x!=-step)
    {
       mx+=step;
       last_action_x=step;
       last_action_y=0;

       this->advance(1);
    }
}

