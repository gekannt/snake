#include "snake_class.h"
#include  "scene.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Updating *item=new Updating;

     QGraphicsScene  graphicscene;
     graphicscene.addItem(item);

    GraphicView view(item,graphicscene);
    view.setScene(&graphicscene);

    view.show();
    return a.exec();
    delete item;
}
