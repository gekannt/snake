#ifndef SCENE_H
#define SCENE_H

#include "inclusion.h"
#include "snake_class.h"
#include "message.h"

class GraphicView: public QGraphicsView
{
   Updating *m_update;
   QGraphicsScene *mscene;
   string mmessage;
   int mtimer_step;
public:
    GraphicView(Updating *update, QGraphicsScene &scene);

    void keyPressEvent(QKeyEvent *event);
   QTimer *timer;



};

#endif // SCENE_H
