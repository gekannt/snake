/****************************************************************************
** Meta object code from reading C++ file 'snake_class.h'
**
** Created: Tue Feb 19 12:30:27 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "snake_class.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'snake_class.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Updating[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   10,    9,    9, 0x0a,
      29,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Updating[] = {
    "Updating\0\0phase\0advance(int)\0move()\0"
};

void Updating::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Updating *_t = static_cast<Updating *>(_o);
        switch (_id) {
        case 0: _t->advance((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->move(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Updating::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Updating::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Updating,
      qt_meta_data_Updating, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Updating::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Updating::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Updating::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Updating))
        return static_cast<void*>(const_cast< Updating*>(this));
    if (!strcmp(_clname, "QGraphicsPathItem"))
        return static_cast< QGraphicsPathItem*>(const_cast< Updating*>(this));
    return QObject::qt_metacast(_clname);
}

int Updating::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
