#include "scene.h"

GraphicView::GraphicView(Updating *update, QGraphicsScene &scene)
{
 m_update=update;
 this->setGeometry(this->x()+10,this->y()+25,   this->geometry().width()+10,this->geometry().height()+10);
 this->setWindowTitle("geksnake");
 mmessage="game is over";
 mscene=&scene;

mtimer_step=600;
timer = new QTimer(m_update);
timer->connect(timer, SIGNAL(timeout()), m_update, SLOT(move()));
timer->start(mtimer_step);
}



void GraphicView::keyPressEvent(QKeyEvent *event)
{
    if ( m_update->should_be_cleared() == true)
    {
      mscene->clear();
      timer->stop();

      Updating *old=m_update;
      m_update=new Updating;


      mscene->addItem(m_update);

      timer = new QTimer(m_update);
      timer->connect(timer, SIGNAL(timeout()), m_update, SLOT(move()));

      timer->start(mtimer_step);

    }

    switch ( event->key())
     {
    case Qt::Key_Up:
       m_update->move_up();
       break;

     case  Qt::Key_Down:
       m_update->move_down();
       break;

     case  Qt::Key_Left:
     {  m_update->move_right();
       break;
    }
     case Qt::Key_Right:
    {  m_update->move_left();
       break;
    }
    default:
        break;
   }

}
