#ifndef DIALOG_MESSAGE_POPUP_WINDOW_H
#define DIALOG_MESSAGE_POPUP_WINDOW_H

#include "ui_dialog_message_popup_window.h"
#include <inclusion.h>
//#include <QDialog>

//#include "ui_dialog.h"


namespace Ui {
class dialog_message_popup_window;
}

class dialog_message_popup_window : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_message_popup_window(QWidget *parent = 0);
    ~dialog_message_popup_window();
    void mshow(QString text);

private slots:
    void on_pushButton_clicked();

private:
    Ui::dialog_message_popup_window *ui;
};



#endif // DIALOG_MESSAGE_POPUP_WINDOW_H
