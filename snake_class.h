#ifndef DIALOG_H
#define DIALOG_H
#include "message.h"
#include "inclusion.h"

class Updating :public QObject, public QGraphicsPathItem
{
  Q_OBJECT
 int mx,my;
 int last_action_x,last_action_y;

 int msizex_ellipse,msizey_ellipse;

 list <QPoint> mgone_path;
 list <QPoint>::iterator iter;

 string mmessage;
 QPainterPath mpainter_path;

  int mcollapsed;
public:
   Updating();


   void move_up();
   void move_down();
   void move_left();
   void move_right();
   void collapse();
   int should_be_cleared();

public slots:
   void advance(int phase);
   void move();




};

#endif // DIALOG_H
